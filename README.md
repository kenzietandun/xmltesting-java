### XML Reading with Java SAX & DOM

Steps:

- Clone this repository with `git clone`

- Import the project to your favourite IDE, I used Eclipse

##### DOM

- Run `DOMReadMenu.java` to test reading xml file with DOM

- Run `DOMWriteMenu.java` to test writing xml file

##### SAX

- Run `SAXMenuReader.java` to test reading xml file with SAX
