public class Menu {
    private String name;
    private int price;
    private int id;

    public void setId(int id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    public String toString() {
        String template = "";
        template += this.id + ": ";
        template += this.name + ", ";
        template += "$ " + this.price + ".";
        
        return template;
    }
}
