import java.io.*;
import java.util.*;

import org.xml.sax.*;
import org.xml.sax.helpers.XMLReaderFactory;

public class SAXMenuReader {

    public ArrayList parseXML(InputStream in) {
        ArrayList<Menu> menus = new ArrayList<Menu>();
        try {
            SAXMenuHandler handler = new SAXMenuHandler();
            XMLReader parser = XMLReaderFactory.createXMLReader();
            parser.setContentHandler(handler);
            InputSource source = new InputSource(in);
            parser.parse(source);
            
            menus = handler.getMenus();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        
        return menus;
    }
    
    public static void main(String[] args) throws Exception {
        File xmlFile = new File("src/menus.xml");
        SAXMenuReader reader = new SAXMenuReader();
        ArrayList menus = reader.parseXML(new FileInputStream(xmlFile));
        for (Object m : menus) {
            System.out.println((Menu) m);
        }
        
    }
}
