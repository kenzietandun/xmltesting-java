import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import org.w3c.dom.*;

public class DOMWriteMenu {

	
	public static void main(String[] args) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document document = builder.parse(new File("src/menus.xml"));

		document.getDocumentElement().normalize();

		// adding new entries to the file
		
		// create new tag menu with id 10
		Element menu = document.createElement("menu");
		Attr attr = document.createAttribute("id");
		attr.setValue("10");
		menu.setAttributeNode(attr);

		// set the menu name to scrambled egg
		Element menuName = document.createElement("name");
		menuName.appendChild(document.createTextNode("scrambled egg"));
		menu.appendChild(menuName);

		// set the menu price to 40
		Element menuPrice = document.createElement("price");
		menuPrice.appendChild(document.createTextNode("40"));
		menu.appendChild(menuPrice);

		// add it as a entry of tag "menus"
		document.getElementsByTagName("menus").item(0).appendChild(menu);

		// see if the new entry has been added
		NodeList nList = document.getElementsByTagName("menu"); 

		for (int temp = 0; temp < nList.getLength(); temp++)
		{
			Node node = nList.item(temp);
			System.out.println("");    //Just a separator
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element eElement = (Element) node;
				System.out.println("Menu id : "    + eElement.getAttribute("id"));
				System.out.println("Name  : "  + eElement.getElementsByTagName("name").item(0).getTextContent());
				System.out.println("Price : "   + eElement.getElementsByTagName("price").item(0).getTextContent());
			}
		}

		// write all the entries into a file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource domSource = new DOMSource(document);
		StreamResult streamResult = new StreamResult(new File("src/menus-new.xml"));
		transformer.transform(domSource, streamResult);
		System.out.println("Done creating XML File");


	}
}
