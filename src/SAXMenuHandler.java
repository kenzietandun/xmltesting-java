/*
 * https://howtodoinjava.com/xml/sax-parser-read-xml-example/
 */

import org.xml.sax.*;
import org.xml.sax.helpers.*;
import java.util.*;

public class SAXMenuHandler extends DefaultHandler {
    
    private Stack elementStack = new Stack();
    private ArrayList menuList = new ArrayList();
    private Stack objectStack = new Stack();
    
    public void startDocument() throws SAXException {}

    public void endDocument() throws SAXException {}

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.elementStack.push(qName);
        
        if ("menu".equals(qName)) {
            Menu menuObject = new Menu();
            
            if (attributes != null && attributes.getLength() == 1) {
                menuObject.setId(Integer.parseInt(attributes.getValue(0)));
            }
            
            this.objectStack.push(menuObject);
        }
    }
    
    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.elementStack.pop();
        
        if ("menu".equals(qName)) {
            Menu object = (Menu) this.objectStack.pop();
            this.menuList.add(object);
        }
    }
    
    /*
     * This will be called every time parser encounter a value node
     * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
     */
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length).trim();
        
        if (value.length() == 0) {
            return;
        }
        
        if ("name".equals(currentElement())) {
            Menu menu = (Menu) this.objectStack.peek();
            menu.setName(value);
        } else if ("price".equals(currentElement())) {
            Menu menu = (Menu) this.objectStack.peek();
            menu.setPrice(Integer.valueOf(value));
        }
    }
    
    private String currentElement() { 
        return (String) this.elementStack.peek();
    }
    
    public ArrayList getMenus() {
        return menuList;
    }
}
