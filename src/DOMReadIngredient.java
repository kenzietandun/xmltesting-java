import javax.xml.parsers.*;
import java.io.*;
import java.util.ArrayList;

import org.w3c.dom.*;

public class DOMReadIngredient {
	public static void main(String[] args) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document document = builder.parse(new File("src/Ingredients.xml"));

		document.getDocumentElement().normalize();

		Element root = document.getDocumentElement();

		// find all entries with tag "menu"
		NodeList nList = document.getElementsByTagName("ingredient"); 
		
		ArrayList<Ingredient> ingredients = new ArrayList<>();
		boolean isGF;
		boolean isVeg;
		boolean isVegan;

		for (int temp = 0; temp < nList.getLength(); temp++)
		{
			Node node = nList.item(temp);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element eElement = (Element) node;
				Ingredient m = new Ingredient();
				isGF = eElement.getAttribute("isgf").equals("yes");
				isVeg = eElement.getAttribute("isveg").equals("yes");
				isVegan = eElement.getAttribute("isvegan").equals("yes");
				m.setUnit(eElement.getAttribute("unit"));
				m.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
				m.setCode(eElement.getElementsByTagName("code").item(0).getTextContent());
				m.setGF(isGF);
				m.setVeg(isVeg);
				m.setVegan(isVegan);
				ingredients.add(m);
			}
		}
		
		for (Ingredient m : ingredients) {
            System.out.println(m);
		}

	}
}
