
public class Ingredient {
    private String name;
    private String code;
    private String unit;
    boolean isGF;
    boolean isVeg;
    boolean isVegan;

    public void setName(String name) {
        this.name = name;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String toString() {
        String template = "";
        template += String.format("%-20s", this.code);
        template += String.format("%-35s", this.name);
        template += String.format("%-10s", this.unit);
        template += String.format("%10s", this.isGF);
        template += String.format("%10s", this.isVeg);
        template += String.format("%10s", this.isVegan);
        return template;
    }

    public void setGF(boolean isGF) {
        this.isGF = isGF;
    }

    public void setVeg(boolean isVeg) {
        this.isVeg = isVeg;
    }

    public void setVegan(boolean isVegan) {
        this.isVegan = isVegan;
    }
}
